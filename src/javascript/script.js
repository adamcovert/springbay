//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/fullpage.js/vendors/jquery.easings.min.js
//= ../../node_modules/fullpage.js/vendors/scrolloverflow.min.js
//= ../../node_modules/fullpage.js/dist/jquery.fullpage.js

$(document).ready(function() {

  $('#fullpage').fullpage({
    menu: '#menu',
    lockAnchors: false,
    anchors:['firstPage', 'secondPage', '3thPage', '4thPage'],
    navigation: false,
    navigationPosition: 'right',
    navigationTooltips: ['firstSlide', 'secondSlide'],
    showActiveTooltip: false,
    slidesNavigation: false,
    slidesNavPosition: 'bottom',

    css3: true,
    scrollingSpeed: 900,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 6000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    dragAndMove: false,
    offsetSections: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: false,
    scrollOverflowReset: false,
    scrollOverflowOptions: null,
    touchSensitivity: 25,
    normalScrollElementTouchThreshold: 5,
    bigSectionsDestination: null,

    responsiveWidth: 992,
  });

  var burger = document.querySelector('.burger');
  var menu = document.querySelector('.fullscreen-nav');
  var win = window;

  function openMenu(event) {
    burger.classList.toggle('burger--is-active');
    menu.classList.toggle('fullscreen-nav--is-open');
    event.preventDefault();
    event.stopImmediatePropagation();
  }

  function closeMenu() {
    if ( menu.classList.contains('fullscreen-nav--is-open') ) {
      menu.classList.remove('fullscreen-nav--is-open');
      burger.classList.remove('burger--is-active');
    }
  }

  burger.addEventListener('click', openMenu, false);
  win.addEventListener('click', closeMenu, false);

});